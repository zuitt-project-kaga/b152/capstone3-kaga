# Capstone 3 Requirements
### Capstone Objective

At the end of the capstone period, the students are expected to:
- integrate the frontend (React.js) with the backend (Express.js API) then deploy the app to Heroku.
<br><br>
# Main Requirements

### In your backend (Capstone 2):
- In your E-commerce API, add a route to get your user's details.  
- In your E-commerce API, add a route to get all products (active/inactive), admin only.
<br><br>

### Create the necessary backend integration using the fetch() function to the following pages:

- [x]  Login

- [x]  Register
- [x]  Add Product
- [x]  Enable Product
- [x]  Disable Product
- [x]  View Active Products (Regular and Guests)
- [x]  Admin Dashboard (All Products view)

<br><br>

### Write the necessary component states and form input bindings for every form found in the React.js app.

- [x]  Login
- [x]  Register
- [x]  Add Product

<br><br>

- If Add Product page components are accessed by a regular user, the app must redirect back to the Login page component.
- If the Login and Register page components are accessed while there is a logged in user, the app must redirect to the Home page component.
- Customized and Personalized (Do not simply copy sample and demo).

<br><br>

# Stretch Requirements (At least 3)

- [x] Add update product feature.

- [x] Add view single product.
- [x] Add cart page.
- [x] Add view user orders
- [x] Add view all orders
- [x] Add checkout to create order
- [x] Feature random items in home page

Backend API for the capstone 3 is your capstone 2.

<br><br>

# Additional Function

- [x] Optimize linking for SPA.
- [ ] Add alert beauty

- [ ] Oprimize Navigation bar to show/unshow with normal/login/admin user. 
- [ ] Decorate better design of each pages with Material UI.
- [ ] Add picture of pdoducts comes from Unsplash.